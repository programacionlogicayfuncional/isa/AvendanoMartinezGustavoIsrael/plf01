(ns plf01.core)

(defn función-<-1
  [a b]
  (< a b))
(defn función-<-2
  [a b]
  (< a b))
(defn función-<-3
  [a b c d]
  (< a b c d))
(función-<-1 4 5)
(función-<-2 1 1/2)
(función-<-3 2 4 6 7)

(defn función-<=-1
  [a b]
  (<= a b))
(defn función-<=-2
  [a b]
  (<= a b))
(defn función-<=-3
  [a b & more]
  (<= a b & more))

(función-<=-1 4/5 3/2)
(función-<=-2 5 7)
(función-<=-3 1/4 1/3 0.5 1)

(defn función-==-1
  [a]
  (== a))
(defn función-==-2
  [a b]
  (== a b))
(defn función-==-3
  [a b & more]
  (== a b & more))

(función-==-1)
(función-==-2 2 3)
(función-==-3 1/1,2/2,3/3,4/4,5/5)

(defn función->-1
  [a]
  (> a))
(defn función->-2
  [a b]
  (> a b))
(defn función->-3
  [a b & more]
  (> a b & more))
(función->-1 4)
(función->-2 1 1/2)
(función->-3 6 5 4 3 2 1 1/2 1/3)

(defn función->=-1
  [a]
  (>= a))
(defn función->=-2
  [a b]
  (>= a b))
(defn función->=-3
  [a b & more]
  (>= a b & more))

(función->=-1 4/5)
(función->=-2 5 7)
(función->=-3 1 0.5 2/4 4/8 1/8)

(defn función-assoc-1
  [map key val]
  (assoc map key val))
(defn función-assoc-2
  [map key val]
  (assoc map key val))
(defn función-assoc-3
  [map key val & kvs]
  (assoc map key val))

(función-assoc-1 userData :age 27)
(función-assoc-2 userData :name "Israel")
(función-assoc-3 userData :userID 1 :name "Israel" :lastname "Avendaño" :age 27)

(defn función-assoc-in-1
  [m [k & ks] v]
  (assoc-in m [k & ks] v))
(defn función-assoc-in-2
  [m [k & ks] v]
  (assoc-in m [k & ks] v))
(defn función-assoc-in-3
  [m [k & ks] v]
  (assoc-in m [k & ks] v))

(función-assoc-in-1 user [1 :age] 27)
(función-assoc-in-2 user [0 :password] "@vendañO234")
(función-assoc-in-3 1 user [2] {:name "Israel" :age 26})

(defn función-concat-1
  [a]
  (concat a))
(defn función-concat-2
  [a b]
  (concat a b))
(defn función-concat-3
  [a b & more]
  (concat a b & more))

(función-concat-1 [4 5])
(función-concat-2 [1 2] [3 4])
(función-concat-3 [1] [2] '(3 4) [5 6 7] #{9 10 8})

(defn función-conj-1
  [coll a]
  (conj coll a))
(defn función-conj-2
  [coll a]
  (conj coll a))
(defn función-conj-3
  [coll a & xs]
  (conj coll a & xs))

(función-conj-1 [1 2 3] 4)
(función-conj-2 ["a" "b" "c"] "d")
(función-conj-3 {:firstname "John" :lastname "Doe"} {:age 25 :nationality "Chinese"} {:hobby "play the instruments"})

(defn función-cons-1
  [x seq]
  (cons x seq))
(defn función-cons-2
  [x seq]
  (cons x seq))
(defn función-cons-3
  [x seq]
  (cons x seq))

(función-cons-1 1 '(2 3 4 5 6))
(función-cons-2 6 '(7 8 9))
(función-cons-3 [1 2] [4 5 6])

(defn función-contains?-1
  [coll key]
  (contains? coll key))
(defn función-contains?-2
  [coll key]
  (contains? coll key))
(defn función-contains?-3
  [coll key]
  (contains? coll key))

(función-contains?-1 {:a 1} :a)
(función-contains?-2 {:a 1} :b)
(función-contains?-3 [:a :b :c] 2)

(defn función-count-1
  [coll]
  (count coll))
(defn función-count-2
  [coll]
  (count coll))
(defn función-count-3
  [coll]
  (count coll))

(función-count-1 [1 2 3])
(función-count-2 {:one 1 :two 2})
(función-count-3  [1 \a "string" [1 2] {:foo :bar}])

(defn función-disj-1
  [set]
  (disj set))
(defn función-disj-2
  [set key]
  (disj set key))
(defn función-disj-3
  [set key & ks]
  (disj set key & ks))

(función-disj-1 #{1 2 3})
(función-disj-2 #{1 2 3} 2)
(función-disj-3 #{1 2 3} 1 3)

(defn función-dissoc-1
  [map]
  (dissoc map))
(defn función-dissoc-2
  [map key]
  (dissoc map key))
(defn función-dissoc-3
  [map key & ks]
  (dissoc set key & ks))

(función-dissoc-1  {:a 1 :b 2 :c 3})
(función-dissoc-2 {:a 1 :b 2 :c 3} :b)
(función-dissoc-3 {:a 1 :b 2 :c 3} :c :b)

(defn función-distinct-1
  [coll]
  (distinct coll))
(defn función-distinct-2
  [coll]
  (distinct coll))
(defn función-distinct-3
  [coll]
  (distinct coll))

(función-distinct-1 [1 2 1 3 1 4 1 5])
(función-distinct-2 [1 1 1 2 2 3 3])
(función-distinct-3 fractions)

(defn función-distinct?-1
  [coll]
  (distinct? coll))
(defn función-distinct?-2
  [coll]
  (distinct? coll))
(defn función-distinct?-3
  [coll]
  (distinct? coll))

(función-distinct?-1 [1 2 1 3 1 4 1 5])
(función-distinct?-2 [1 1 1 2 2 3 3])
(función-distinct?-3 1 2 3 1)

(defn función-drop-last-1
  [coll]
  (drop-last coll))
(defn función-drop-last-2
  [n coll]
  (drop-last n coll))
(defn función-drop-last-3
  [n coll]
  (drop-last n coll))

(función-drop-last-1 [1 2 3 4])
(función-drop-last-2 5 [1 2 3 4])
(función-drop-last-3 2 {:a 1 :b 2 :c 3 :d 4})

(defn función-empty-1
  [coll]
  (empty coll))
(defn función-empty-2
  [coll]
  (empty coll))
(defn función-empty-3
  [coll]
  (empty coll))

(función-empty-1 '(1 2))
(función-empty-2 #{1 2})
(función-empty-3 [1 2])

(defn función-empty?-1
  [coll]
  (empty? coll))
(defn función-empty?-2
  [coll]
  (empty? coll))
(defn función-empty?-3
  [coll]
  (empty? coll))

(función-empty?-1 ())
(función-empty?-2 '())
(función-empty?-3 ["" [] () '() {} #{} nil])

(defn función-even?-1
  [a]
  (even? a))
(defn función-even?-2
  [a]
  (even? a))
(defn función-even?-3
  [a]
  (even? a))

(función-even?-1 2)
(función-even?-2 (range 10))
(función-even?-3 1)

(defn función-false?-1
  [n]
  (false? n))
(defn función-false?-2
  [n]
  (false? n))
(defn función-false?-3
  [n]
  (false? n))

(función-false?-1 false)
(función-false?-2 true)
(función-false?-3  "foo")

(defn función-find-1
  [map key]
  (find map key))
(defn función-find-2
  [map key]
  (find map key))
(defn función-find-3
  [map key]
  (find map key))

(función-find-1 {:a 1 :b 2 :c 3} :a)
(función-find-2 [:a :b :c :d] 2)
(función-find-3 {:a 1 :b 2 :c 3} :d)

(defn función-first-1
  [coll]
  (first coll))
(defn función-first-2
  [coll]
  (first coll))
(defn función-first-3
  [coll]
  (first coll))

(función-first-1 '(:alpha :bravo :charlie))
(función-first-2 [1 2])
(función-first-3 [[1 2] [3 4]])

(defn función-flatten-1
  [x]
  (flatten x))
(defn función-flatten-2
  [x]
  (flatten x))
(defn función-flatten-3
  [x]
  (flatten x))

(función-flatten-1 '(1 2 3))
(función-flatten-2 '(1 2 [3 (4 5)]))
(función-flatten-3 (seq {:name "Hubert" :age 23}))

(defn función-frequencies-1
  [coll]
  (frequencies coll))
(defn función-frequencies-2
  [coll]
  (frequencies coll))
(defn función-frequencies-3
  [coll]
  (frequencies coll))

(función-frequencies-1 ['a 'b 'a 'a])
(función-frequencies-2 [3 6 2 6 8 7 'b 'c 3 5 3 4 7 6 'a])
(función-frequencies-3 [3 6 2 6 8 7 'b 'c])

(defn función-get-1
  [map key]
  (get map key))
(defn función-get-2
  [map key]
  (get map key))
(defn función-get-3
  [map key not-found]
  (get map key not-found))

(función-get-1 [1 2 3] 1)
(función-get-2 {:a 1 :b 2} :b)
(función-get-3 (hash-map :a 1 :b 2) "a" "not found")

(defn función-get-in-1
  [map ks]
  (get-in map ks))
(defn función-get-in-2
  [map ks]
  (get-in map ks))
(defn función-get-in-3
  [map ks not-found]
  (get-in map ks not-found))

(función-get-in-1 map [:profile :name])
(función-get-in-2 map [:pets 1 :type])
(función-get-in-3 map [:profile :address :zip-code] "no zip code!")

(defn función-into-1
  [to]
  (into to))
(defn función-into-2
  [to from]
  (into to from))
(defn función-into-3
  [to xform from]
  (into to xform from))

(función-into-1 (sorted-map) [{:a 1} {:c 3} {:b 2}])
(función-into-2 [1 2 3] '(4 5 6))
(def xform (comp (map #(+ 2 %))
                 (filter odd?)))
(función-into-3 [-1 -2] xform (range 10))

(defn función-key-1
  [e]
  (key e))
(defn función-key-2
  [e]
  (key e))
(defn función-key-3
  [e]
  (key e))

(función-key-1 {:a 1 :b 2})
(función-key-2 (clojure.lang.MapEntry. :a :b)
(función-key-3 {:foo :bar})
               
(defn función-keys-1
  [map]
  (keys map))
(defn función-keys-2
  [map]
  (keys map))
(defn función-keys-3
  [map]
  (keys map))

(función-keys-1 {:keys :and, :some :values})
(función-keys-2 {})
(función-keys-3 nil)

(defn función-max-1
  [x]
  (max x))
(defn función-max-2
  [x y]
  (max x y))
(defn función-max-3
  [x y & more]
  (max x y & more))

(función-max-1 100)
(función-max-2 41 90)
(función-max-3 5 4 3 2 1)

(defn función-merge-1
  [pred-forms]
  (merge pred-forms))
(defn función-merge-2
  [pred-forms]
  (merge pred-forms))
(defn función-merge-3
  [pred-forms]
  (merge pred-forms))

(función-merge-1 ::person ::address)
(función-merge-2 [nil {:a 1} {:b 2}]
(función-merge-3 ::street ::city)

(defn función-min-1
  [x]
  (min x))
(defn función-min-2
  [x y]
  (min x y))
(defn función-min-3
  [x y & more]
  (min x y & more))

(función-min-1 100)
(función-min-2 41 90)
(función-min-3 5 4 3 2 1)

(defn función-neg?-1
  [x]
  (neg? x))
(defn función-neg?-2
  [x]
  (neg? x))
(defn función-neg?-3
  [x]
  (neg? x))

(función-neg?-1 100.21)
(función-neg?-2 -55)
(función-neg?-3 -1/2)

(defn función-nil?-1
  [x]
  (nil? x))
(defn función-nil?-2
  [x]
  (nil? x))
(defn función-nil?-3
  [x]
  (nil? x))

(función-nil?-1 nil)
(función-nil?-2 0)
(función-nil?-3 false)

(defn función-not-empty-1
  [coll]
  (not-empty coll))
(defn función-not-empty-2
  [coll]
  (not-empty coll))
(defn función-not-empty-3
  [coll]
  (not-empty coll))

(función-not-empty-1 [3])
(función-not-empty-2 [])
(función-not-empty-3 [4 11 9])

(defn función-nth-1
  [coll index]
  (nth coll index))
(defn función-nth-2
  [coll index]
  (nth coll index))
(defn función-nth-3
  [coll index not-found]
  (nth coll index not-found))

(función-nth-1 my-seq 0)
(función-nth-2 my-seq 3)
(función-nth-3 ["last"] -1 "this is not perl")

(defn función-odd?-1
  [x]
  (odd? x))
(defn función-odd?-2
  [x]
  (odd? x))
(defn función-odd?-3
  [x]
  (odd? x))

(función-odd?-1 21)
(función-odd?-2 80)
(función-odd?-3 0)

(defn función-partition-1
  [n coll]
  (partition n coll))
(defn función-partition-2
  [n step coll]
  (partition n step coll))
(defn función-partition-3
  [n step pad coll]
  (partition n step pad coll))

(función-partition-1 4 (range 20))
(función-partition-2 4 6 (range 20))
(función-partition-3 4 6 ["a" "b" "c" "d"] (range 20))

(defn función-partition-all-1
  [n]
  (partition-all n))
(defn función-partition-all-2
  [n coll]
  (partition-all n coll))
(defn función-partition-all-3
  [n step coll]
  (partition-all n step coll))

(función-partition-all-1 3)
(función-partition-all-2 4 [0 1 2 3 4 5 6 7 8 9])
(función-partition-all-3 2 4 [0 1 2 3 4 5 6 7 8 9])

(defn función-peek-1
  [coll]
  (peek coll))
(defn función-peek-2
  [coll]
  (peek coll))
(defn función-peek-3
  [coll]
  (peek coll))

(función-peek-1 [1 2 3 4])
(función-peek-2 '(1 2 3 4))
(función-peek-3 '())

(defn función-pop-1
  [coll]
  (pop coll))
(defn función-pop-2
  [coll]
  (pop coll))
(defn función-pop-3
  [coll]
  (pop coll))

(función-pop-1 [1 2 3 4])
(función-pop-2 '(1 2 3 4))
(función-pop-3 '(56 57 8 89 4))

(defn función-pos?-1
  [num]
  (pos? num))
(defn función-pos?-2
  [num]
  (pos? num))
(defn función-pos?-3
  [num]
  (pos? num))

(función-pos?-1 1.0)
(función-pos?-2 0)
(función-pos?-3 -1/2)

(defn función-quot-1
  [num div]
  (quot num div))
(defn función-quot-2
  [num div]
  (quot num div))
(defn función-quot-3
  [num div]
  (quot num div))

(función-quot-1 12.3 5)
(función-quot-2 10 3)
(función-quot-3 -5.9 3)

(defn función-range-1
  [end]
  (range end))
(defn función-range-2
  [start end]
  (range start end))
(defn función-range-3
  [start end step]
  (range start end step))

(función-range-1 11)
(función-range-2 -5 5)
(función-range-3 -100 100 5)

(defn función-rem-1
  [num div]
  (rem num div))
(defn función-rem-2
  [num div]
  (rem num div))
(defn función-rem-3
  [num div]
  (rem num div))

(función-rem-1 10 9)
(función-rem-2 2 2)
(función-rem-3 -10 3)

(defn función-repeat-1
  [x]
  (repeat x))
(defn función-repeat-2
  [n x]
  (repeat n x))
(defn función-repeat-3
  [n x]
  (repeat n x))

(función-repeat-1 "x")
(función-repeat-2 "I" 7)
(función-repeat-3 "#" 11)

(defn función-replace-1
  [smap]
  (replace smap))
(defn función-replace-2
  [smap coll]
  (replace smap coll))
(defn función-replace-3
  [smap coll]
  (replace smap coll))

(función-replace-1 {2 :two, 4 :four})
(función-replace-2 [10 9 8 7 6] [0 2 4])
(función-replace-3 [:zeroth :first :second :third :fourth] [0 2 4 0])

(defn función-rest-1
  [coll]
  (rest coll))
(defn función-rest-2
  [coll]
  (rest coll))
(defn función-rest-3
  [coll]
  (rest coll))

(función-rest-1 ["a" "b" "c" "d" "e"])
(función-rest-2 [1 2 3 4 5])
(función-rest-3 #{1 2 3 4 5})

(defn función-select-keys-1
  [map keyseq]
  (select-keys map keyseq))
(defn función-select-keys-2
  [map keyseq]
  (select-keys map keyseq))
(defn función-select-keys-3
  [map keyseq]
  (select-keys map keyseq))

(función-select-keys-1 {:a 1 :b 2} [:a])
(función-select-keys-2 {:a 1 :b 2} [:a :c])
(función-select-keys-3 [1 2 3] [0 0 2])

(defn función-shuffle-1
  [coll]
  (shuffle coll))
(defn función-shuffle-2
  [coll]
  (shuffle coll))
(defn función-shuffle-3
  [coll]
  (shuffle coll))

(función-shuffle-1 [1 2 3])
(función-shuffle-2 (list 1 2 3))
(función-shuffle-3 [1 2])

(defn función-sort-1
  [coll]
  (sort coll))
(defn función-sort-2
  [comp coll]
  (sort comp coll))
(defn función-sort-3
  [comp coll]
  (sort comp coll))

(función-sort-1 [3 1 2 4])
(función-sort-2 (comp - compare) [[1 0] [0 0] [0 3] [2 1]])
(función-sort-3 #(compare %2 %1) [[1 0] [0 0] [0 3] [2 1]])

(defn función-split-at-1
  [n coll]
  (split-at n coll))
(defn función-split-at-2
  [n coll]
  (split-at n coll))
(defn función-split-at-3
  [n coll]
  (split-at n coll))

(función-split-at-1 2 [1 2 3 4 5])
(función-split-at-2 3 [1 2])
(función-split-at-3 3 [1 2 4 6 6 7 8])

(defn función-str-1
  []
  (str))
(defn función-str-2
  [x]
  (str x))
(defn función-str-3
  [x & ys]
  (str x & ys))

(función-str-1)
(función-str-2 2 4 5 6)
(función-str-3 1 'symbol :keyword)

(defn función-subs-1
  [s start]
  (subs s start))
(defn función-subs-2
  [s start end]
  (subs s start end))
(defn función-subs-3
  [s start end]
  (subs s start end))

(función-subs-1 "Clojure" 1)
(función-subs-2 "Clojure" 1 3)
(función-subs-3 1 "Gustavo Israel Avendaño" 8 13)

(defn función-subvec-1
  [v start]
  (subvec v start))
(defn función-subvec-2
  [v start end]
  (subvec v start end))
(defn función-subvec-3
  [v start end]
  (subvec v start end))

(función-subvec-1 [1 2 3 4 5 6 7] 2)
(función-subvec-2 [1 2 3 4 5 6 7] 2 4)
(función-subvec-3 [1 2 3 5 8] 0 2)

(defn función-take-1
  [n]
  (take n))
(defn función-take-2
  [n coll]
  (take n coll))
(defn función-take-3
  [n coll]
  (take n coll))

(función-take-1 2)
(función-take-2 3 '(1 2 3 4 5 6))
(función-take-3 3 [1 2 3 4 5])

(defn función-true?-1
  [x]
  (true? x))
(defn función-true?-2
  [x]
  (true? x))
(defn función-true?-3
  [x]
  (true? x))

(función-true?-1 true)
(función-true?-2 1)
(función-true?-3 (= 1 1))

(defn función-val-1
  [e]
  (val e))
(defn función-val-2
  [e]
  (val e))
(defn función-val-3
  [e]
  (val e))

(función-val-1 {:a 1 :b 2})
(función-val-2 (first {:one :two}))
(función-val-3 (clojure.lang.MapEntry. :a :b))

(defn función-vals-1
  [map]
  (vals map))
(defn función-vals-2
  [map]
  (vals map))
(defn función-vals-3
  [map]
  (vals map))

(función-vals-1 {:a "foo", :b "bar"})
(función-vals-2 {})
(función-vals-3 nil)

(defn función-zero?-1
  [num]
  (zero? num))
(defn función-zero?-2
  [num]
  (zero? num))
(defn función-zero?-3
  [num]
  (zero? num))

(función-zero?-1 3.14159265358)
(función-zero?-2 (/ 1 2))
(función-zero?-3 0.0)

(defn función-zipmap-1
  [keys vals]
  (zipmap keys vals))
(defn función-zipmap-2
  [keys vals]
  (zipmap keys vals))
(defn función-zipmap-3
  [keys vals]
  (zipmap keys vals))

(función-zipmap-1 [:a :b :c :d :e] [1 2 3 4 5])
(función-zipmap-2 [:a :b :c] [1 2 3 4])
(función-zipmap-3 [:a :b :c] [1 2])
